<?php
/**
 * @file
 * Rewards processor interface - this interface should be implemented by
 * Reward classes provided by other modules.
 * @see commerce_rewards_db_processor module for an example.
 */

interface RewardInterface {
  public function form($form, &$form_state, $checkout_pane, $order);
  public function getBalance();
  public function redeemReward($amount);
  public function generateReward();
  public function payload();
  public function rules_invoke($line_item);
}

