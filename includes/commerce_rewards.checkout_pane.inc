<?php
/**
 * @file
 * Rewards checkout form callback.
 */

/**
 * Implements base_checkout_form().
 */
function commerce_rewards_pane_checkout_form($form, &$form_state, $checkout_pane, $order) {
  // Get enabled reward processor base class name.
  $form = array();
  if (user_is_anonymous()) {
    return;
  }
  $class = commerce_rewards_get_class();
  if (!empty($class)) {
    $giftcard = new $class;
    $form = $giftcard->form($form, $form_state, $checkout_pane, $order);
  }
  return $form;
}
