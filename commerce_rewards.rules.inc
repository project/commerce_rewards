<?php

/**
 * @file
 * Commerce Rewards Rules integration.
 */

/**
 * Implements hook_rules_action_info()
 */
function commerce_rewards_rules_action_info() {
  $actions = array(
    'commerce_rewards_rules_generate_reward_account' => array(
      'label' => t('Generate reward account'),
      'group' => t('Commerce Rewards'),
      'parameter' => array(
        'account' => array(
          'type' => 'user',
          'label' => t('User Account'),
        ),
        'balance' => array(
          'type' => 'decimal',
          'label' => t('Opening balance'),
        ),
      ),
    ),
    'commerce_rewards_rules_add_amount_to_balance' => array(
      'label' => t('Add amount to rewards account balance'),
      'group' => t('Commerce Rewards'),
      'parameter' => array(
        'account_id' => array(
          'type' => 'integer',
          'label' => t('Rewards account ID'),
        ),
        'amount' => array(
          'type' => 'decimal',
          'label' => t('Amount'),
          'decription' => t('Use negative amount to decrease rewards account balance'),
        ),
      ),
    ),
  );
  return $actions;
}

/**
 * Generate new Rewards account for user account.
 */
function commerce_rewards_rules_generate_reward_account($account, $amount) {
  $amount *= 100;
  $rewards_processor = commerce_rewards_get_class();
  $reward_acc = new $rewards_processor(array('owner_id' => $account->id));
  $reward_acc->generateReward(intval($amount));
}

/**
 * Adjust rewards account balance.
 */
function commerce_rewards_rules_add_amount_to_balance($rewards_account_id, $amount) {
  $amount *= 100;
  $rewards_processor = commerce_rewards_get_class();
  $reward_acc = new $rewards_processor(array('owner_id' => $rewards_account_id));
  $reward_acc->generateReward(intval($amount));
}
