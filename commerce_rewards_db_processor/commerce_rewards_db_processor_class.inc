<?php
/**
 * @file
 * Core functionality of the reward processor is defined in a class implementing
 * RewardInterface interface.
 */

class DrupalReward implements RewardInterface {
  private $reward_number;
  private $reward_balance;

  function __construct($variables = array()) {
    global $user;
    $number = isset($variables['reward_number']) ? $variables['reward_number'] : $user->uid;
    $this->reward_number = $number;
    $this->reward_balance = 0;
    if ($this->checkValidation()) {
      $this->reward_balance = $this->getBalance();
    }
    elseif ($this->reward_number) {
      $this->generateReward();
    }
  }

  public function form($form, &$form_state, $checkout_pane, $order) {
    $form['commerce_rewards'] = array(
      '#type' => 'container',
      '#prefix' => '<div id="commerce_rewards_container">',
      '#suffix' => '</div>',
      '#tree' => FALSE,
    );
    $form['commerce_rewards']['commerce_reward_pane_fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => t('Rewards account information'),
      '#tree' => TRUE,
    );
    $form['commerce_rewards']['commerce_reward_pane_fieldset']['commerce_reward_pane_reward_number'] = array(
      '#type' => 'hidden',
      '#value' => $this->reward_number,
      '#required' => TRUE,
    );
    $form['commerce_rewards']['commerce_reward_pane_fieldset']['commerce_reward_balance_container'] = array(
      '#type' => 'container',
      '#prefix' => '<div id="commerce_reward_balance_container">',
      '#suffix' => '</div>',
      '#tree' => FALSE,
    );
    $form['commerce_rewards']['commerce_reward_pane_fieldset']['commerce_reward_balance_container']['commerce_reward_pane_balance'] = array(
      '#type' => 'item',
      '#title' => t('Account balance') . ': ',
      '#markup' => commerce_currency_format($this->getBalance(), commerce_default_currency()),
    );

    $form['commerce_rewards']['use_rewards'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use rewards balance for payment'),
      '#disabled' => $this->reward_balance ? FALSE : TRUE,
      '#ajax' => array(
        'wrapper' => 'checkout_review_form',
        'callback' => 'commerce_rewards_db_processor_owed_balance_callback',
      ),
    );
    $form['commerce_rewards']['payment_info'] = array(
      '#type' => 'container',
      '#prefix' => '<div id="commerce_rewards_balance_owed">',
      '#suffix' => '</div>',
    );
    return $form;
  }

  public function setBalance($amount) {
    if (!$this->reward_number) {
      return FALSE;
    }
    $this->reward_balance = $amount;
    db_update('commerce_rewards_db')->fields(array('reward_balance' => $this->reward_balance))
            ->condition('reward_number', $this->reward_number)->execute();
    return $this->reward_balance;
  }

  public function getBalance() {
    $query = db_select('commerce_rewards_db', 'reward')
            ->condition('reward_number', $this->reward_number)
            ->fields('reward', array('reward_balance'));
    $balance = $query->execute()->fetchField();
    if (!$balance) {
      $balance = 0;
    }
    $this->reward_balance = $balance;
    return $this->reward_balance;
  }

  public function redeemReward($amount) {
    $this->reward_balance -= $amount;
    db_update('commerce_rewards_db')->fields(array('reward_balance' => $this->reward_balance))
            ->condition('reward_number', $this->reward_number)->execute();
    return $this->reward_balance;
  }

  public function generateReward($amount = 0) {
    if (!$this->reward_number) {
      global $user;
      $this->reward_number = $user->uid;
    }

    if ($this->checkValidation()) {
      $this->getBalance();
      $this->reward_balance += $amount;
      db_update('commerce_rewards_db')->fields(array(
        'reward_balance' => $this->reward_balance,
      ))->condition('reward_number', $this->reward_number)->execute();
    }
    else {
      $this->reward_balance = $amount;
      db_insert('commerce_rewards_db')->fields(array(
        'reward_number' => $this->reward_number,
        'reward_balance' => $this->reward_balance,
      ))->execute();
    }
  }

  private function checkValidation() {
    if ($this->reward_number == 0) {
      return FALSE;
    }
    $query = db_select('commerce_rewards_db', 'reward')
        ->condition('reward_number', $this->reward_number)
        ->fields('reward', array('reward_number'));
    $number = $query->execute()->fetchField();

    if ($number == $this->reward_number){
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  public function payload() {
    return array(
      'reward_number' => $this->reward_number,
    );
  }

  public function rules_invoke($line_item) {
    $send_method = $line_item->commerce_reward_send_method[LANGUAGE_NONE][0]['value'];
    if($send_method == 'email') {
      $send_method = 'mail';
    }
    $from_name = $line_item->commerce_reward_from_name[LANGUAGE_NONE][0]['value'];
    $to_name = $line_item->commerce_reward_to_name[LANGUAGE_NONE][0]['value'];
    $from = $line_item->{'commerce_reward_from_' . $send_method}[LANGUAGE_NONE][0]['value'];
    $to = $line_item->{'commerce_reward_to_' . $send_method}[LANGUAGE_NONE][0]['value'];
    rules_invoke_event('reward_generate', $send_method, $from_name, $to_name, $from, $to, $this->reward_number, $this->reward_balance);
  }
}
