<?php

/**
 * @file
 * Rules integration for Commerce Rewards Database Processor backend.
 */


/**
 * Implements hook_rules_event_info().
 */
function commerce_rewards_db_processor_rules_event_info() {
  $items = array(
    'reward_generate' => array(
      'label' => t('After Generating a Reward'),
      'group' => t('Commerce Reward'),
      'variables' => array(
        'send_method' => array(
          'label' => t('Send Method'),
          'type' => 'text',
        ),
        'from_name' => array(
          'label' => t('Sender Name'),
          'type' => 'text',
        ),
        'to_name' => array(
          'label' => t('Receiver Name'),
          'type' => 'text',
        ),
        'from' => array(
          'label' => t('Sender mobile/email/address'),
          'type' => 'text',
        ),
        'to' => array(
          'label' => t('Receiver mobile/email/address'),
          'type' => 'text',
        ),
        'reward_number' => array(
          'label' => t('Reward Number'),
          'type' => 'text',
        ),
        'reward_balance' => array(
          'label' => t('Reward Balance'),
          'type' => 'text',
        ),
      ),
    ),
  );
  return $items;
}
